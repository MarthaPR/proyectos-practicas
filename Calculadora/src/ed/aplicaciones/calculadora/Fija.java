/*
 * Código utilizado para el curso de Estructuras de Datos.
 * Se permite consultarlo para fines didácticos en forma personal,
 * pero no está permitido transferirlo tal cual a estudiantes actuales o potenciales.
 */
package ed.aplicaciones.calculadora;

import java.util.Stack;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Clase para evaluar expresiones en notaciones prefija y postfija.
 *
 * @author blackzafiro
 */
public class Fija {

	/**
	 * Evalúa la operación indicada por <code>operador</code>.
	 */
	private static double evalua(char operador, double operando1, double operando2) {
		switch (operador) {
			case '+': return operando1 + operando2;
			case '-': return operando1 - operando2;
			case '*': return operando1 * operando2;
			case '/': return operando1 / operando2;
			case '%': return operando1 % operando2;
		  //case '()': return (operando1,  operando2);
			default:
				System.err.println("OPERADOR INVALIDO");
		}
		return -1;
	}

	/**
	 * Recibe la secuencia de símbolos de una expresión matemática en notación
	 * prefija y calcula el resultado de evaluarla.
	 *
	 * @param tokens Lista de símbolos: operadores y números.
	 * @return resultado de la operación.
	 */
	public static double evaluaPrefija(String[] tokens) {
		Stack<String> p = new Stack<>();
		for (int i = tokens.length-1; i >= 0 ; i-- ) {
			if (esNumero(tokens[i])){
				p.push(tokens[i]);
			}else{
				double operando1 = Double.parseDouble(p.pop());
				double operando2 = Double.parseDouble(p.pop());
				char operador = tokens[i].charAt(0);
				double operacion = evalua(operador, operando1, operando2);
				p.push(Double.toString(operacion));
			}
		}
		return Double.parseDouble(p.pop());
	}

	//Metodo auxiliar para revisar si la cadena es un numero
	public static boolean esNumero(String num) {
    try {
        double d = Double.parseDouble(num);
    } catch (NumberFormatException nfe) {
        return false;
    }
    return true;
	}

	/**
	 * Recibe la secuencia de símbolos de una expresión matemática en notación
	 * postfija y calcula el resultado de evaluarla.
	 *
	 * @param tokens Lista de símbolos: operadores y números.
	 * @return resultado de la operación.
	 */
	public static double evaluaPostfija(String[] tokens) {
		Stack<String> p = new Stack<>();
		for (int i = 0; i < tokens.length ; i++ ) {
			if (esNumero(tokens[i])){
				p.push(tokens[i]);
			}else{
				double operando1 = Double.parseDouble(p.pop());
				double operando2 = Double.parseDouble(p.pop());
				char operador = tokens[i].charAt(0);
				double operacion = evalua(operador, operando2, operando1);
				p.push(Double.toString(operacion));
			}
		}
		return Double.parseDouble(p.pop());
	}

	/**
	 * Interfaz de texto para la calculadora.
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String sentence;
		String method = "prefija";
		String delims = "\\s+|(?<=\\()|(?=\\))";
		String[] tokens;
		while (true) {
			sentence = scanner.nextLine();
			switch (sentence) {
				case "exit":
					return;
				case "prefija":
				case "postfija":
					System.out.println("Cambiando a notación " + sentence);
					method = sentence;
					continue;
				default:
					break;
			}
			tokens = sentence.split(delims);
			System.out.println(Arrays.toString(tokens));
			if (method.equals("postfija")) {
				System.out.println("= " + evaluaPostfija(tokens));
			} else {
				System.out.println("= " + evaluaPrefija(tokens));
			}

		}
	}
}
