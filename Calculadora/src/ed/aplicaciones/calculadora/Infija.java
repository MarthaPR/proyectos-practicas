/*
 * Código utilizado para el curso de Estructuras de Datos.
 * Se permite consultarlo para fines didácticos en forma personal,
 * pero no está permitido transferirlo tal cual a estudiantes actuales o potenciales.
 */
package ed.aplicaciones.calculadora;

import java.util.Stack;
import java.util.Arrays;
import java.util.Queue;
import java.util.Scanner;
import java.util.LinkedList;

/**
 * Clase para evaluar expresiones en notación infija.
 *
 * @author blackzafiro
 */
public class Infija {

	/**
	 * Devuelve la precedencia de cada operador. Entre mayor es la precedencia,
	 * más pronto deberá ejecutarse la operación.
	 *
	 * @operador Símbolo que representa a las operaciones: +,-,*,/ y '('.
	 * @throws UnsupportedOperationException para cualquier otro símbolo.
	 */
	private static int precedencia(char operador) {
		switch (operador) {
			case '+': return 1;
			case '-': return 1;
			case '*': return 2;
			case '/': return 2;
			case '%': return 2;
			case '(': return 0;
			default:
				throw new UnsupportedOperationException("OPERADOR INVALIDO");
		}
	}

	/**
	 * Pasa las operaciones indicadas en notación infija a notación sufija o
	 * postfija.
	 *
	 * @param tokens Arreglo con símbolos de operaciones (incluyendo paréntesis)
	 * y números (según la definición aceptada por
	 * <code>Double.parseDouble(token)</code> en orden infijo.
	 * @return Arreglo con símbolos de operaciones (sin incluir paréntesis) y
	 * números en orden postfijo.
	 */
	public static String[] infijaASufija(String[] tokens) {
		Stack<String> pl = new Stack<>();
		Queue<String> qu = new LinkedList<>();
		for (int i = 0; i < tokens.length ; i++ ){
			if (Fija.esNumero(tokens[i])) {
				qu.offer(tokens[i]);
			} else {
				if (tokens[i].charAt(0) == ')') {
					while (!pl.isEmpty() && pl.peek().charAt(0) != '(') {
						qu.offer(pl.pop());
					}
					if (!pl.isEmpty())
						pl.pop();

				}else {
					int prec = precedencia(tokens[i].charAt(0));
					while (!pl.isEmpty() && precedencia(pl.peek().charAt(0)) >= prec) {
						qu.offer(pl.pop());
					}
						pl.push(tokens[i]);
				}

			}//else

		}//for
		while (!pl.isEmpty()) {
			qu.offer(pl.pop());
		}
		String[] arr = new String[qu.size()];
		arr = qu.toArray(arr);
		return arr;
	}

	/**
	 * Recibe la secuencia de símbolos de una expresión matemática en notación
	 * infija y calcula el resultado de evaluarla.
	 *
	 * @param tokens Lista de símbolos: operadores, paréntesis y números.
	 * @return resultado de la operación.
	 */
	public static double evaluaInfija(String[] tokens) {
		String[] suf = infijaASufija(tokens);
		System.out.println("Sufija: " + Arrays.toString(suf));
		return Fija.evaluaPostfija(suf);
	}

	/**
	 * Interfaz de texto para la calculadora.
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String sentence;
		String method = "infija";
		String delims = "\\s+|(?<=\\()|(?=\\))";
		String[] tokens;

		System.out.println("Para cambiar a notacion prefija escriba 'prefija'");
		System.out.println("Para cambiar a notacion postfija escriba 'postfija'");
		System.out.println("Para regresar a notación infija escriba 'infija'");
		System.out.println("Para terminar escriba 'exit'");
		System.out.println("--------------------------------------------------");
		System.out.println("Calculadora en modo notación " + method);
		System.out.println("Ingrese la expresion a evaluar");
		while (true) {
			sentence = scanner.nextLine();
			switch (sentence) {
				case "exit":
					return;
				case "infija":
				case "prefija":
				case "postfija":
					System.out.println("Cambiando a notación " + sentence);
					System.out.println("Ingrese la expresion a evaluar");
					method = sentence;
					continue;
				default:
					break;
			}
			tokens = sentence.split(delims);
			System.out.println("Tokens: " + Arrays.toString(tokens));
			double resultado;
			switch (method) {
				case "infija":
					resultado = evaluaInfija(tokens);
					break;
				case "prefija":
					resultado = Fija.evaluaPrefija(tokens);
					break;
				case "postfija":
					resultado = Fija.evaluaPostfija(tokens);
					break;
				default:
					System.out.println("Método inválido <" + method
							+ "> seleccione alguno de:\n"
							+ "\tinfija\n"
							+ "\tprefija\n"
							+ "\tpostfija\n");
					continue;
			}
			System.out.println("= " + resultado);
			System.out.println("Para terminar escriba 'exit'");
			System.out.println("Ingrese la expresion a evaluar");
		}
	}
}
