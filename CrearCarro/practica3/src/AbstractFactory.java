/**
* Clase abstracta.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public abstract class AbstractFactory {

  /**
  * Metodo que regresa el tipo de componente.
  * @param tipoComponente componente del carro.
  */
  public abstract Object getComponente(String tipoComponente);

}
