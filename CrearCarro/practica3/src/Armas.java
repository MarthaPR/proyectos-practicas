/**
* Clase para la fabrica de Armas.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Armas extends AbstractFactory {

  /**
  * Metodo que regresa el tipo de componente.
  */
  @Override
	public Object getComponente(String tipoComponente){
		return getArma(tipoComponente);
	}

  /**
  * Metodo que regresa el tipo de arma.
  */
  public Arma getArma(String tipoArma){
    if(tipoArma == null){
      return null;
    }
    String car = tipoArma.toLowerCase();
    switch (car) {
      case "arpon":
        return new Arpon();
      case "sierra":
        return new Sierra();
      case "metralleta":
        return new Metralleta();
      case "cañon":
        return new Canon();
      case "lanzallamas":
        return new Lanzallamas();
      default:
        return null;
    }
  }
}//class
