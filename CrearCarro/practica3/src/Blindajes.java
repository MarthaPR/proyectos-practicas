/**
* Clase para la fabrica de Blindajes.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Blindajes extends AbstractFactory {

  /**
  * Metodo que regresa el tipo de componente.
  */
  @Override
	public Object getComponente(String tipoComponente){
		return getBlindaje(tipoComponente);
	}

  /**
  * Metodo que regresa el tipo de blindaje.
  */
  public Blindaje getBlindaje(String tipoBlindaje){
    if(tipoBlindaje == null){
      return null;
    }
    String car = tipoBlindaje.toLowerCase();
    switch (car) {
      case "simple":
        return new Bsimple();
      case "tanque":
        return new Tanque();
      case "reforzado":
        return new Reforzado();
      default:
        return null;
    }
  }
}//class
