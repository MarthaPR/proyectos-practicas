/**
* Interface para tipo de carroceria.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public interface Carroceria {

  /**
  * Metodo para crear el carroceria.
  */
  public void creaCarroceria();

  /**
  * Metodo que regresa el tipo de carroceria.
  */
  public void getTipo();

  /**
  * Metdo para el costo del carroceria.
  * @return costo del carroceria.
  */
  public double getCosto();

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa();

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque();

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad();


}//clas
