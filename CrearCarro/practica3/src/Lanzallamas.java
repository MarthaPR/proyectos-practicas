/**
* Clase para arma lanzallamas.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Lanzallamas implements Arma {

  /*Variable para guardar el tipo de arma*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Lanzallamas(){
    tipo = "Lanzallamas";
  }

  /**
  * Metodo para crear el arma.
  */
  public void creaArma(){
    tipo = "Lanzallamas";
  }

  /**
  * Metodo que regresa el tipo de arma.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo del arma.
  * @return el costo del arma.
  */
  public double getCosto(){
    return 100.25;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 1;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 6;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 10;
  }

}//clas
