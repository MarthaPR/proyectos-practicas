/**
* Clase para arma Metralleta.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Metralleta implements Arma {

  /*Varibale para guardar el tipo de arma*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Metralleta(){
    tipo = "Metralleta";
  }

  /**
  * Metodo para crear el arma.
  */
  public void creaArma(){
    tipo = "Metralleta";
  }

  /**
  * Metodo que regresa el tipo de arma.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo del arma.
  * @return el costo del arma.
  */
  public double getCosto(){
    return 73.45;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 2;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 9;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 5;
  }

}//clas
