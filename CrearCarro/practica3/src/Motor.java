/**
* Interface para tipo de motor.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public interface Motor {

  /**
  * Metodo para crear el motor.
  */
  public void creaMotor();

  /**
  * Metodo que regresa el tipo de motor.
  */
  public void getTipo();

  /**
  * Metdo para el costo del motor.
  * @return costo del motor.
  */
  public double getCosto();

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa();

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque();

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad();


}//clas
