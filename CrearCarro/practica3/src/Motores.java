/**
* Clase para la fabrica de motores.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Motores extends AbstractFactory {

  /**
  * Metodo que regresa el tipo de componente.
  */
  @Override
	public Object getComponente(String tipoComponente){
		return getMotor(tipoComponente);
	}

  /**
  * Metodo que regresa el tipo de motor.
  */
  public Motor getMotor(String tipoMotor){
    if(tipoMotor == null){
      return null;
    }
    String car = tipoMotor.toLowerCase();
    switch (car) {
      case "diesel":
        return new Diesel();
      case "deportivo":
        return new Deportivo();
      case "turbo":
        return new Turbo();
      default:
        return null;

    }
  }
}//class
