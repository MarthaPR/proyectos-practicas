/**
* Clase para la llanta simple.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Simple implements Llanta {

  /*Variable para guardar el tipo de llanta*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Simple(){
    tipo = "Llanta Simple";
  }

  /**
  * Metodo para crear la llanta.
  */
  public void creaLlanta(){
    tipo = "Llanta Simple";
  }

  /**
  * Metodo que regresa el tipo de llanta.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo de la llanta.
  * @return el costo de la llanta.
  */
  public double getCosto(){
    return 113.12;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 4;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 2;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 4;
  }


}//clas
