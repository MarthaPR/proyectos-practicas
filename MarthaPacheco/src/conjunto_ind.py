
# Clases Vertice, Arista y Grafica.
# @author Martha Y. Pacheco Ramirez.
# @version 1.0.


# Clase vértice.
class Vertice:

    # Constructor
    # num - El número de vértice
    def __init__(self, num):
        self.num = num

    # Método para representar el vértice en cadena.
    def __str__(self):
        return 'vértice número ' + str(self.num)

#Clase arista.
class Arista:

    # Constructor.
    # entra - En que vertice entra la arista.
    # sale - De que vertice sale la arista.
    def __init__(self, sale, entra):
        self.entra = entra
        self.sale = sale

    # Metodo para respresentar la arista en cadena.
    def __str__(self):
        return 'La arista sale del ' + srt(self.sale) + ' y entra al ' + str(self.entra)

#Clase grafica.
class Grafica:

    # v - lista vertices.
    # a - lista de aristas.
    def __init__(self, v, a):
        self.v = v
        self.a = a

    # Metodo para regresar la grfica en cadena.
    def __str__(self):
        return 'Grafica G = ' + 'los vertices de G son ' + str(self.v)
        + 'las aristas de G son ' + str(self.a)

    # Metodo para saber si hay una arista entre dos vertices.
    # v1 - primer vertice.
    # v2 - segundo vertice.
    def hay_arista(self, v1, v2):
        for n in self.a:
            if n.sale == v1 and n.entra == v2:
                return True
        return False

    # Metdo para saber si dos vertices estan conectados.
    # v1 - vertice uno.
    # v2 - vertices dos
    def estan_conect(self, v1, v2):
        return self.hay_arista(v1,v2) or self.hay_arista(v2,v1)

    # Metodo para saber la vecindad del vertice es completa.
    # v - vertice a ver su vecindad
    def vencidad_compl(self, v):
        for n in self.v:
            if n != v and not self.hay_arista(v, n):
                return False
        return True

    # Metodo para saber la vecindad del vertice.
    # v - vertice a obtener vecindad.
    def vencidad_v(self, v):
        l = [v]
        for n in self.a:
            if n.sale == v:
                l.append(n.entra)
        return l

    # Metodo para saber si los vertices son independientes.
    # lista de vertices.
    def indep(self, l):
        for a in l:
            for b in l:
                if self.estan_conect(a,b):
                    return False
        return True

    # Metodo para conectar dos vertices.
    # n1 - num del vertice a conectar.
    # n1 - num del vertice a conectar.
    def conecta(self,n1,n2):
        w = p = None
        for a in self.v:
            if n1 == a.num:
                w = a
            elif n2 == a.num:
                p = a
        n = Arista(w,p)
        self.a.append(n)


################################################
# Metodo para obtener la difencia entre dos listas de vertices.
# l1 lista uno
# l2 lista dos
def diferencia(l1, l2):
    l = []
    for a in l1:
        if not (a in l2):
            l.append(a)
    return l

# Metodo para quitar a los vertices de la lista
# l1 lista uno
# l2 lista dos
def quita(l1, l2):
    l = []
    for a in l1:
        if not (a.entra in l2 or a.sale in l2):
            l.append(a)
    return l

# g - grafica que recibe la funcion.
# l - lo que regresa la funcion, es una lista de vertices, del conjunto
# independiente.
def conj_ind(g):
    if len(g.v) == 1:
        return g.v
    elif len(g.v) == 2:
        v1,v2 = g.v[0], g.v[1]
        if not g.hay_arista(v1,v2) and not g.hay_arista(v2,v1):
            return g.v
        elif g.hay_arista(v2,v1):
            return [v2]
        else:
            return [v1]
    elif len(g.v) == 3:
        v1,v2,v3 = g.v[0],g.v[1],g.v[2]
        #caso: salen del mismo vertice las aristas.
        if g.hay_arista(v1,v2) and g.hay_arista(v1,v3):
            return [v1]
        elif g.hay_arista(v2,v1) and g.hay_arista(v2,v3):
            return [v2]
        elif g.hay_arista(v3,v1) and g.hay_arista(v3,v2):
            return [v3]
        #caso: una arista sale y una entra.
        elif g.hay_arista(v3,v1) and g.hay_arista(v1,v2):
            return [v3]
        elif g.hay_arista(v1,v2) and g.hay_arista(v2,v3):
            return [v1]
        elif g.hay_arista(v2,v3) and g.hay_arista(v3,v1):
            return [v2]
        elif g.hay_arista(v2,v1) and g.hay_arista(v1,v3):
            return [v2]
        elif g.hay_arista(v1,v3) and g.hay_arista(v3,v2):
            return [v1]
        elif g.hay_arista(v3,v2) and g.hay_arista(v2,v1):
            return [v3]
        # caso: dos aristas caen en un mismo vertice
        elif g.hay_arista(v3,v1) and g.hay_arista(v2,v1):
            return [v2,v3]
        elif g.hay_arista(v1,v3) and g.hay_arista(v2,v3):
            return [v1,v2]
        elif g.hay_arista(v1,v2) and g.hay_arista(v3,v2):
            return [v1,v3]
        #caso: un vertice desconectado.
        elif g.estan_conect(v1,v2):
            return [v1,v3]
        elif g.estan_conect(v1,v3):
            return [v1,v2]
        elif g.estan_conect(v2,v3):
            return [v2,v1]
        #caso: grafica disconexa
        else:
            return g.v
    else:
        v = g.v[0]
        if g.vencidad_compl(v):
            return [v]
        else:
            vecin = g.vencidad_v(v)
            lv = diferencia(g.v, vecin)
            la = quita(g.a, vecin)
            h = Grafica(lv,la)
            sh = conj_ind(h)
            lh = sh[:]
            lh.append(v)
            if g.indep(lh):
                return lh
            else:
                return sh

leer = input("Escribe el nombre del archivo: ")
f = open(leer, "r")
Lines = f.readlines()
g = Lines[0].split(",")
lV = []
lA = []
for n in g:
    v = Vertice(int(n))
    lV.append(v)
grafica = Grafica(lV,lA)

for n in Lines[1:]:
    l = n.split(",")
    grafica.conecta(int(l[0]),int(l[1]))

print("El conjunto independiente es: ")
for n in conj_ind(grafica):
    print(n)
