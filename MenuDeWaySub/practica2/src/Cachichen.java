/**
* Clase para las pizzas.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Cachichen extends PizzaAdaptador {

  /*Constructor de clase*/
  public Cachichen(){

  }

  /**
  * Metodo que regresa el tipo de baguette.
  * @return regresa el tipo de pan y el ingrediente que se agrego.
  */
  public String getTipo(){
    return "Cachichen";
  }

  /**
  * Metodo que regresa el tipo de Queso.
  */
  public String getQueso(){
    return "Chedar";
  }

  /**
  * Metodo que regresa el tipo de Carne.
  */
  public String getCarne(){
    return "Salchicha";
  }

  /**
  * Metodo que regresa el tipo de Masa.
  */
  public String getMasa(){
    return "Masa Gruesa";
  }

  /**
  * Metodo para el costo por el tipo de pan.
  * @return el costo del producto.
  */
  @Override
  public double costo(){
    return 13.00;
  }

  /**
  * Metodo que calcula el total de la compra.
  * @return total de compra.
  */
  @Override
  public double getTotal(){
    return costo();
  }


}//class
