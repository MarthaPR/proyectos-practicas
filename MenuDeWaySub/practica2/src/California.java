/**
* Clase para las pizzas.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class California extends PizzaAdaptador {

  /*Constructor de clase*/
  public California(){

  }

  /**
  * Metodo que regresa el tipo de baguette.
  * @return regresa el tipo de pan y el ingrediente que se agrego.
  */
  public String getTipo(){
    return "California";
  }

  /**
  * Metodo que regresa el tipo de Queso.
  */
  public String getQueso(){
    return "Chedar";
  }

  /**
  * Metodo que regresa el tipo de Carne.
  */
  public String getCarne(){
    return "Pollo";
  }

  /**
  * Metodo que regresa el tipo de Masa.
  */
  public String getMasa(){
    return "Masa Delgada";
  }

  /**
  * Metodo para el costo por el tipo de pan.
  * @return el costo del producto.
  */
  @Override
  public double costo(){
    return 14.00;
  }

  /**
  * Metodo que calcula el total de la compra.
  * @return total de compra.
  */
  @Override
  public double getTotal(){
    return costo();
  }


}//class
