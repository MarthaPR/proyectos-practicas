/**
* Clase abstracta para la clase decorador.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public abstract class IngredienteDecorador extends Baguette {

  /**
  * Metodo que regresa el tipo de baguette.
  */
  public String getTipo(){
    return tipoBa;
  }

  /**
  * Metodo que regresa el costo del ingrediente.
  * @return el costo del producto.
  */
  public abstract double costo();

  /**
  * Metodo que calcula el total de la compra.
  * @return total de compra.
  */
  public abstract double getTotal();

  /**
  * Metdo que imprime la compra.
  */
  public abstract void imprimeCompra();


}
