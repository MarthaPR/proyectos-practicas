/**
* Clase para el tipo de pan Italiano.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Italiano extends Baguette {

  /*Atributo para guardar */


  /**
  * Constructor de clase.
  */
  public Italiano(){
    tipoBa = "Italiano";
  }

  /**
  * Metodo para el costo por el tipo de pan.
  * @return el costo del producto.
  */
  @Override
  public double costo(){
    return 1.80;
  }

  /**
  * Metodo que calcula el total de la compra.
  * @return total de compra.
  */
  @Override
  public double getTotal(){
    return costo();
  }

  /**
  * Metodo para imprimir el costo.
  */
  @Override
  public void imprimeCompra(){
    System.out.println("Pan Italiano " + "--- " + costo());
  }

}//class
