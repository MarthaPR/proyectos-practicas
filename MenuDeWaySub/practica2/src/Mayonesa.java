/**
* Clase para ingrediente.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Mayonesa extends IngredienteDecorador {

  /*Atributo para guardar el tipo de baguette*/
  Baguette baguette;

  public Mayonesa(Baguette baguette){
    this.baguette = baguette;
  }

  /**
  * Metodo que regresa el tipo de baguette.
  * @return regresa el tipo de pan y el ingrediente que se agrego.
  */
  public String getTipo(){
    return baguette.getTipo() + " Mayonesa" ;
  }

  /**
  * Metodo para el costo por el tipo de pan.
  * @return el costo del producto.
  */
  @Override
  public double costo(){
    return .20;
  }

  /**
  * Metodo que calcula el total de la compra.
  * @return total de compra.
  */
  @Override
  public double getTotal(){
    return costo() + baguette.getTotal();
  }

  /**
  * Metodo para imprimir el costo.
  */
  @Override
  public void imprimeCompra(){
    System.out.println("Mayonesa " + "--- " + costo());
    baguette.imprimeCompra();
  }

}//class
