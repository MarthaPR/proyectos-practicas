/**
* Interface para adapter.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public abstract class PizzaAdaptador extends Baguette{

  /**
  * Metodo que regresa el tipo de Queso.
  */
  public abstract String getQueso();

  /**
  * Metodo que regresa el tipo de Carne.
  */
  public abstract String getCarne();

  /**
  * Metodo que regresa el tipo de Masa.
  */
  public abstract String getMasa();

  /**
  * Metodo que regresa el tipo de bagutte.
  * @return el tipo de pan que elige.
  */
  public String getTipo(){
    return tipoBa;
  }

  /**
  * Metodo que regresa el costo del ingrediente.
  * @return el costo del producto.
  */
  public abstract double costo();

  /**
  * Metodo que calcula el total de la compra.
  * @return total de compra.
  */
  public abstract double getTotal();

  /**
  * Metdo que imprime la compra.
  */
  public void imprimeCompra(){
    System.out.println("Ticket: \n" + getTipo()+ " --- " + getTotal() + "\n" +
    getQueso() + "\n" + getCarne() + "\n" + getMasa());
  }

}
