/**
* Clase para el tipo de pan Veggie.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Veggie extends Baguette {

  /*Atributo para guardar */


  /**
  * Constructor de clase.
  */
  public Veggie(){
    tipoBa = "Veggie";
  }

  /**
  * Metodo para el costo por el tipo de pan.
  * @return el costo del producto.
  */
  @Override
  public double costo(){
    return 2.00;
  }

  /**
  * Metodo que calcula el total de la compra.
  * @return total de compra.
  */
  @Override
  public double getTotal(){
    return costo();
  }

  /**
  * Metodo para imprimir el costo.
  */
  @Override
  public void imprimeCompra(){
    System.out.println("Pan Veggie " + "--- " + costo());
  }

}//class
