/**
* Clase para crear el Ejercito.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public abstract class BuildEjercito {

  /*Atributo para el peloton 1*/
  protected Peloton peloton1;
  /*Atributo para el peloton 1*/
  protected Peloton peloton2;
  /*Atributo para el peloton 1*/
  protected Peloton peloton3;

  /**
  * Metoto que regresa al peloton 1.
  */
  public Peloton getPeloton1(){
    return peloton1;
  }

  /**
  * Metoto que regresa al peloton 2.
  */
  public Peloton getPeloton2(){
    return peloton2;
  }

  /**
  * Metoto que regresa al peloton 3.
  */
  public Peloton getPeloton3(){
    return peloton3;
  }


}//clas
