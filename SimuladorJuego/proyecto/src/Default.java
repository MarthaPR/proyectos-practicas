/**
* Clase para el Ejercito.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Default extends BuildEjercito {

  /**
  * Constructor de la clase.
  */
  public Default(){
    peloton1 = new Peloton1ED();
    peloton2 = new Peloton2D();
    peloton3 = new Peloton3ED();
  }

}//class
