import java.util.ArrayList;
import java.util.List;

/**
* Clase para el Ejercito.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Ejercito{
  /*Atributo para el peloton 1*/
  private Peloton peloton1;
  /*Atributo para el peloton 2*/
  private Peloton peloton2;
  /*Atributo para el peloton 3*/
  private Peloton peloton3;

  /**
  * Constructor de la Clase
  */
  public Ejercito(BuildEjercito ejercito){
    this.peloton1 = ejercito.getPeloton1();
    this.peloton2 = ejercito.getPeloton2();
    this.peloton3 = ejercito.getPeloton3();
  }

  /**
  * Metoto que regresa al peloton 1.
  */
  public Peloton getPeloton1(){
    return peloton1;
  }

  /**
  * Metoto que regresa al peloton 2.
  */
  public Peloton getPeloton2(){
    return peloton2;
  }

  /**
  * Metoto que regresa al peloton 3.
  */
  public Peloton getPeloton3(){
    return peloton3;
  }

  /**
  *
  */
  public void getDescripcion(){
     System.out.println("El ejercito esta formado por " + "\n" +
                        "Peloton 1 ");
                        peloton1.getComandante().instruirReport();
                        System.out.println();
    System.out.println("Peloton 2 " );
                        peloton2.getComandante().instruirReport();
                        System.out.println();
    System.out.println("Peloton 3 " );
                        peloton3.getComandante().instruirReport();
  }


}//class
