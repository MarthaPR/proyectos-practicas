/**
* Interfaz para la especialidad de soldados.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public interface Especialidad {

  /**
  * Metodo para que los soldados se muevan.
  */
  public abstract void moverse();

  /**
  * Metodo para que los soldados ataquen.
  * @param enemigo para reducir el hp del enemigo.
  */
  public abstract void atacar(Enemigo enemigo);

  /**
  * Metodo para que los soldados se reporten.
  * @param id identificador del soldado.
  * @param nombre nombre del soldado.
  */
  public abstract  void reportar(String id, String nombre);

}
