/**
* Clase para el Ejercito explorador.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Explorador extends BuildEjercito {

  /**
  * Constructor de la clase.
  */
  public Explorador(){
    peloton1 = new Peloton1ED();
    peloton2 = new Peloton2E();
    peloton3 = new Peloton3ED();
  }

}
