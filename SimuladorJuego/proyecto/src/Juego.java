import java.util.Scanner;

/**
* Clase Main.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Juego {
  /*Atributo para el enemigo*/
  public static Enemigo enemigo = new Enemigo();

  /**
  * Metodo pora crear un menu de opcion de la especialidad del Soldado.
  */
  public static void menu(Peloton peloton, Scanner sc){
    System.out.println("1. Atacar" + "\n" +
                       "2. Avanzar" + "\n" +
                       "3. Reportarse");
    int opcion = sc.nextInt();
    switch (opcion) {
      case 1:
        peloton.getComandante().instruirAtaque();
        break;
      case 2:
        peloton.getComandante().instruirMov();
        break;
      case 3:
        peloton.getComandante().instruirReport();
        break;
      default:
        System.out.println("OPCION INVALIDA");
    } //switch
  }

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int opcion;
    System.out.println("¿Qué ejercito quieres?");
		System.out.println("1.- Explorador: ");
    System.out.println("Se conforma con 3 pelotones:");
    System.out.println("Pelotón 1: tienen 6 soldados de infantería");
    System.out.println("Pelotón 2: se compone con 1 artillero y 2 miembros de la caballería");
    System.out.println("Pelotón 3: se compone con 6 soldados, todos miembros de la caballería");
	  System.out.println("2.- Default");
    System.out.println("Se conforma con 3 pelotones");
    System.out.println("Pelotón 1: tienen 6 soldados de infantería");
    System.out.println("Pelotón 2: tiene 3 artilleros");
    System.out.println("Pelotón 3: tiene 6 miembros de la caballería");
		System.out.println("3.- Kamikaze");
    System.out.println("Se conforma con 3 pelotones");
    System.out.println("Pelotón 1 y 2 tienen 5 soldados cada uno, todos miembros de la infantería");
    System.out.println("Pelotón 3 tiene 5 soldados, todos miembros de la caballería");

    opcion = sc.nextInt();
    Ejercito ejercito = null;
    switch (opcion) {
      case 1:
        Explorador explorador = new Explorador();
        ejercito  = new Ejercito(explorador);
        break;
      case 2:
        Default def = new Default();
        ejercito = new Ejercito(def);
        break;
      case 3:
        Kamikaze kamikaze = new Kamikaze();
        ejercito = new Ejercito(kamikaze);
        break;
      default:
        System.out.println("OPCION INVALIDA");
    }
    ejercito.getDescripcion();
    System.out.println();
    System.out.println("Vida del enemigo: " + enemigo.getHPE());
    System.out.println("Distancia del enemigue: " + "10 metros");
    do {
      System.out.println("¿Que deas ordenarle al peloton 1?");
      menu(ejercito.getPeloton1(), sc);
      if (enemigo.getHPE() == 0) {
        break;
      }
      System.out.println("¿Que deas ordenarle al peloton 2?");
      menu(ejercito.getPeloton2(), sc);
      if (enemigo.getHPE() == 0) {
        break;
      }
      System.out.println("¿Que deas ordenarle al peloton 3?");
      menu(ejercito.getPeloton3(), sc);
      System.out.println("Vida restante del enemigo: " + enemigo.getHPE());
    } while (enemigo.getHPE() > 0);

     System.out.println("Felicidades haz ganado");

  }//main
}//class
