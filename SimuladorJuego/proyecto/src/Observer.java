/**
* Interfaz para los observadores.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public interface Observer{

  /**
  * Metodo para notificar a los soldados.
  */
  public void update(String estrategia);


}//class
