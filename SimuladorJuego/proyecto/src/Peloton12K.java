/**
* Clase para el Peloton.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Peloton12K extends Peloton {

  /**
  * Constructor de la clase.
  */
  public Peloton12K(){
    String[] nombre = NombresAleatorios.generarNombresAleatorios(5);
    comandante = new Comandante(nombre[0], new Infanteria());
    peloton.add(comandante);
    peloton.add(new Soldado(nombre[1], new Infanteria()));
    peloton.add(new Soldado(nombre[2], new Infanteria()));
    peloton.add(new Soldado(nombre[3], new Infanteria()));
    peloton.add(new Soldado(nombre[4], new Infanteria()));
    comandante.setSoldados(peloton);
  }
}
