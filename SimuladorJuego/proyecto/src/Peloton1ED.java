/**
* Clase para el Peloton.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Peloton1ED extends Peloton {

  /**
  * Constructor de la clase.
  */
  public Peloton1ED(){
    String[] nombre = NombresAleatorios.generarNombresAleatorios(6);
    comandante = new Comandante(nombre[0], new Infanteria());
    peloton.add(comandante);
    peloton.add(new Soldado(nombre[1], new Infanteria()));
    peloton.add(new Soldado(nombre[2], new Infanteria()));
    peloton.add(new Soldado(nombre[3], new Infanteria()));
    peloton.add(new Soldado(nombre[4], new Infanteria()));
    peloton.add(new Soldado(nombre[5], new Infanteria()));
    comandante.setSoldados(peloton);
  }

}//class
