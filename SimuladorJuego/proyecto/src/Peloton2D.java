/**
* Clase para el Peloton.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Peloton2D extends Peloton {

  /**
  * Constructor de la clase.
  */
  public Peloton2D(){
    String[] nombre = NombresAleatorios.generarNombresAleatorios(3);
    comandante = new Comandante(nombre[0], new Artilleria());
    peloton.add(comandante);
    peloton.add(new Soldado(nombre[1], new Artilleria()));
    peloton.add(new Soldado(nombre[2], new Artilleria()));
    comandante.setSoldados(peloton);
  }
}
