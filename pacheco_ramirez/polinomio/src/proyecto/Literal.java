package proyecto;

/**
* Clase para las literales del Monomio.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/
public class Literal{

  /*grado de la literal*/
  private int grado;
  /*parte literal del Monomio*/
  private char literal;

  /**
  * Constructor de la clase.
  * @param grado grado de la literal.
  * @param literal parte literal del Monomio.
  */
  public Literal(int grado, char literal){
    this.grado = grado;
    this.literal = literal;
  }

  /**
  * Metodo que regresa el grado.
  * @return grado del monomio.
  */
  public int getGrado(){
    return grado;
  }

  /**
  * Metodo que regresa la literal del monomio.
  * @return literal del monomio.
  */
  public char getLiteral(){
    return literal;
  }

  /**
  * Metodo para regresar en cadena las literales del monomio.
  * @return cadena de literales.
  */
  @Override
  public String toString(){
    if (grado == 0) {
      return "";
    } else if (grado == 1) {
      return "" + literal;
      } else {
      return "" + literal + "^" + grado;
        }
  }

  /**
  * Metodo para para comparar las literales y grados.
  * @param o literal con la que se va a comparar.
  * @return <code>true</code> si son iguales, <code>false</code> si no.
  */
  @Override
  public boolean equals(Object o){
    if(o == null || !(o instanceof Literal)){
      return false;
    }
    //cast a o para poder comparar.
    Literal l = (Literal) o;

    if (l.grado == 0 && grado == 0) {
      return true;
    } else if (l.grado == grado && l.literal == literal) {
      return true;
    }
    return false;

  }

}//class Literal
