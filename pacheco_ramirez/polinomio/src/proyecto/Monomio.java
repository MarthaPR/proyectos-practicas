package proyecto;

import java.util.LinkedList;

/**
* Clase para Monomio.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/
public class Monomio{

  /*variable para los coeficientes del Monomio*/
  private int cofi;
  /*lista para los elementos del Monomio*/
  private LinkedList<Literal> literales;

  /**
  * Constructor de la clase
  * @param cofi variable para los coeficientes del Monomio.
  * @param literales lista para los elementos del Monomio.
  */
  public Monomio(int cofi, LinkedList<Literal> literales){
    this.cofi = cofi;
    this.literales = literales;
  }

  /**
  * Metodo que regresa el coeficiente.
  * @return coeficiente del monomio.
  */
  public int getCofi(){
    return cofi;
  }

  /**
  * Metodo que regresa una representacion en cadena del monomio.
  * @return cadena del monomio.
  */
  @Override
  public String toString(){
    //variable a regresar
    String mo = "";
    mo += cofi;
    for (Literal a : literales) {
      if (a.getGrado() == 0) {
        mo += a.toString();
      } else
        mo += a.toString() + " ";
    }
    return mo;
  }

  /**
  * Metodo que compara las literales y grado del monomio con otro monomio.
  * @param m monomio a comparar.
  * @return <code>true</code> si son iguales, <code>false</code> si no.
  */
  public boolean compatible(Monomio m){
    if (literales.size() == m.literales.size()) {
      //variable que nos dice si son compatibles.
      boolean i = literales.containsAll(m.literales);
      return i;
    }
    return false;
  }

  /**
  * Metodo para sumar dos monomios.
  * @param m monomio a sumar.
  * @return la suma de monomios.
  * @throws IllegalArgumentException cuando los monomios no se pueden sumar.
  */
  public Monomio suma(Monomio m) throws IllegalArgumentException{
    if (m.compatible(this)) {
      //variable para guardar la suma de coeficientes.
      int s = m.cofi + this.cofi;
      //variable para copiar la lista de literales.
      LinkedList<Literal> l = new LinkedList<>(literales);
      //variable con la suma de los monomios.
      Monomio mo = new Monomio(s, l);
      return mo;
    }
    throw new IllegalArgumentException("Monomios no compatibles para sumar");
  }

  /**
  * Metodo para para comparar dos monomios.
  * @param o monomio para comparar.
  * @return <code>true</code> si son iguales, <code>false</code> si no.
  */
  @Override
  public boolean equals(Object o){
    if(o == null || !(o instanceof Monomio)){
      return false;
    }
    //cast a o para poder comparar.
    Monomio l = (Monomio) o;
    if (l.compatible(this) && l.cofi == this.cofi) {
      return true;
    } return false;
  }

  /**
  * Metodo para para multiplicar dos monomios.
  * @param m monomio a multiplicar.
  * @return Un nuevo monomio.
  */
  public Monomio multiplica(Monomio m){
    //variable para guardar el nuevo coeficiente del monomio.
    int i = m.cofi * this.cofi;
    //variable para guardar la nueva lista de literales.
    LinkedList<Literal> li = new LinkedList<Literal>();
    li.addAll(m.literales);
    li.addAll(this.literales);

    //variable para guardar las literales multiplicadas.
    LinkedList<Literal> l1 = new LinkedList<Literal>();
    while (!li.isEmpty()) {
      //variable para copiar la lista de literales.
      LinkedList<Literal> l = new LinkedList<>(li);
      //variable para guardar las literales semejantes.
      Literal mo = null;
      for (Literal a : l){
        if (mo == null){
          mo = a;
          li.remove(a);
        } else if (a.getLiteral() == mo.getLiteral()){
            mo = new Literal(a.getGrado() + mo.getGrado(), a.getLiteral());
            li.remove(a);
          }
      }
      if (mo.getGrado() != 0){
        l1.add(mo);
      }
    }
    //variable para el nuevo monomio.
    Monomio mo = new Monomio(i, l1);
    return mo;
  }//multiplica


}//class
