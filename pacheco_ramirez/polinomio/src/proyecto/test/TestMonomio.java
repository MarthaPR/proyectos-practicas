package proyecto.test;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import proyecto.Monomio;
import proyecto.Literal;
import java.util.LinkedList;


/**
 * Clase para pruebas unitarias de la clase {@link Monomio}.
 */
public class TestMonomio {

    /** Expiración para que ninguna prueba tarde más de 5 segundos. */
    @Rule public Timeout expiracion = Timeout.seconds(5);

    /**
     * Prueba unitaria para {@link Monomio#toString}.
     */
    @Test
    public void testToString(){
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      l1.add(new Literal(3, 'x'));
      l1.add(new Literal(6, 'y'));
      Monomio mo = new Monomio(5, l1);
      Assert.assertEquals(mo.toString(), "5x^3 y^6 " );
      System.out.println(mo + "primer monomio");

      LinkedList<Literal> l2 = new LinkedList<Literal>();
      l2.add(new Literal(0, 'x'));
      l2.add(new Literal(10, 'z'));
      Monomio mo1 = new Monomio(7, l2);
      Assert.assertEquals(mo1.toString(), "7z^10 " );
      System.out.println(mo1 + "segundo monomio");

      LinkedList<Literal> l3 = new LinkedList<Literal>();
      l3.add(new Literal(1, 'y'));
      l3.add(new Literal(3, 'z'));
      l3.add(new Literal(5, 'x'));
      l3.add(new Literal(6, 'j'));
      Monomio mo2 = new Monomio(15, l3);
      Assert.assertEquals(mo2.toString(), "15y z^3 x^5 j^6 " );
      System.out.println(mo2 + "tercer monomio");

      LinkedList<Literal> l4 = new LinkedList<Literal>();
      l4.add(new Literal(0, 'x'));
      l4.add(new Literal(0, 'y'));
      l4.add(new Literal(0, 'z'));
      l4.add(new Literal(1, 'w'));
      Monomio mo3 = new Monomio(7, l4);
      Assert.assertEquals(mo3.toString(), "7w " );
      System.out.println(mo3 + "cuarto monomio");
    }//

    /**
     * Prueba unitaria para {@link Monomio#compatible}.
     */
    @Test
    public void testCompatible(){
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      l1.add(new Literal(3, 'x'));
      l1.add(new Literal(4, 'y'));
      l1.add(new Literal(1, 'z'));
      Monomio mo = new Monomio(5, l1);
      LinkedList<Literal> l2 = new LinkedList<Literal>();
      l2.add(new Literal(1, 'z'));
      l2.add(new Literal(3, 'x'));
      l2.add(new Literal(4, 'y'));
      Monomio mo1 = new Monomio(4, l2);
      Assert.assertTrue(mo.compatible(mo1));

      LinkedList<Literal> l3 = new LinkedList<Literal>();
      l3.add(new Literal(1, 'x'));
      l3.add(new Literal(1, 'y'));
      l3.add(new Literal(2, 'z'));
      Monomio mo3 = new Monomio(5, l3);
      LinkedList<Literal> l4 = new LinkedList<Literal>();
      l4.add(new Literal(1, 'x'));
      l4.add(new Literal(1, 'y'));
      l4.add(new Literal(1, 'z'));
      Monomio mo4 = new Monomio(6, l4);
      Assert.assertFalse(mo3.compatible(mo4));

      LinkedList<Literal> l5 = new LinkedList<Literal>();
      l5.add(new Literal(1, 'x'));
      l5.add(new Literal(1, 'y'));
      Monomio mo5 = new Monomio(5, l5);
      LinkedList<Literal> l6 = new LinkedList<Literal>();
      l6.add(new Literal(1, 'x'));
      Monomio mo6 = new Monomio(3, l6);
      Assert.assertFalse(mo5.compatible(mo6));

      LinkedList<Literal> l7 = new LinkedList<Literal>();
      l7.add(new Literal(1, 'x'));
      Monomio mo7 = new Monomio(3, l7);
      LinkedList<Literal> l8 = new LinkedList<Literal>();
      l8.add(new Literal(1, 'x'));
      l8.add(new Literal(1, 'y'));
      Monomio mo8 = new Monomio(5, l8);
      Assert.assertFalse(mo7.compatible(mo8));
    }

    /**
     * Prueba unitaria para {@link Monomio#suma}.
     */
    @Test
    public void testSuma(){
      LinkedList<Literal> l = new LinkedList<Literal>();
      l.add(new Literal(2, 'x'));
      l.add(new Literal(1, 'y'));
      Monomio mo = new Monomio(5, l);
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      l1.add(new Literal(2, 'x'));
      l1.add(new Literal(1, 'y'));
      Monomio mo1 = new Monomio(6, l1);
      LinkedList<Literal> l2 = new LinkedList<Literal>();
      l2.add(new Literal(2, 'x'));
      l2.add(new Literal(1, 'y'));
      Monomio mo2 = new Monomio(11, l2);
      Assert.assertEquals(mo.suma(mo1), mo2);

      LinkedList<Literal> l3 = new LinkedList<Literal>();
      l3.add(new Literal(2, 'x'));
      l3.add(new Literal(1, 'y'));
      Monomio mo3 = new Monomio(2, l2);
      LinkedList<Literal> l4 = new LinkedList<Literal>();
      l4.add(new Literal(2, 'x'));
      l4.add(new Literal(1, 'y'));
      Monomio mo4 = new Monomio(-2, l2);
      LinkedList<Literal> l5 = new LinkedList<Literal>();
      l5.add(new Literal(2, 'x'));
      l5.add(new Literal(1, 'y'));
      Monomio mo5 = new Monomio(0, l2);
      Assert.assertEquals(mo3.suma(mo4), mo5);

    }

    /**
     * Prueba unitaria para {@link Monomio#equals}.
     */
    @Test
    public void testEquals(){
      LinkedList<Literal> l = new LinkedList<Literal>();
      l.add(new Literal(2, 'x'));
      l.add(new Literal(1, 'y'));
      Monomio mo = new Monomio(3, l);
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      l1.add(new Literal(1, 'y'));
      l1.add(new Literal(2, 'x'));
      Monomio mo1 = new Monomio(3, l1);
      Assert.assertEquals(mo, mo1);

      LinkedList<Literal> l2 = new LinkedList<Literal>();
      l2.add(new Literal(1, 'x'));
      l2.add(new Literal(1, 'y'));
      Monomio mo2 = new Monomio(3, l2);
      LinkedList<Literal> l3 = new LinkedList<Literal>();
      l3.add(new Literal(2, 'x'));
      l3.add(new Literal(1, 'y'));
      Monomio mo3 = new Monomio(3, l3);
      Assert.assertNotEquals(mo2, mo3);

      LinkedList<Literal> l4 = new LinkedList<Literal>();
      l4.add(new Literal(1, 'x'));
      l4.add(new Literal(1, 'y'));
      Monomio mo4 = new Monomio(2, l4);
      LinkedList<Literal> l5 = new LinkedList<Literal>();
      l5.add(new Literal(1, 'x'));
      l5.add(new Literal(1, 'y'));
      Monomio mo5 = new Monomio(1, l5);
      Assert.assertNotEquals(mo4, mo5);
    }

    /**
     * Prueba unitaria para {@link Monomio#multiplica}.
     */
    @Test
    public void testMultiplica(){
      LinkedList<Literal> l = new LinkedList<Literal>();
      l.add(new Literal(2, 'l'));
      l.add(new Literal(1, 'x'));
      Monomio mo = new Monomio(3, l);
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      l1.add(new Literal(5, 'l'));
      l1.add(new Literal(1, 'y'));
      Monomio mo1 = new Monomio(2, l1);
      LinkedList<Literal> l2 = new LinkedList<Literal>();
      l2.add(new Literal(7, 'l'));
      l2.add(new Literal(1, 'x'));
      l2.add(new Literal(1, 'y'));
      Monomio mo2 = new Monomio(6, l2);
      Assert.assertEquals(mo.multiplica(mo1), mo2);

      LinkedList<Literal> l3 = new LinkedList<Literal>();
      l3.add(new Literal(1, 'x'));
      l3.add(new Literal(1, 'y'));
      l3.add(new Literal(3, 'z'));
      Monomio mo3 = new Monomio(2, l3);
      LinkedList<Literal> l4 = new LinkedList<Literal>();
      l4.add(new Literal(1, 'z'));
      l4.add(new Literal(1, 'x'));
      l4.add(new Literal(1, 'a'));
      Monomio mo4 = new Monomio(1, l4);
      LinkedList<Literal> l5 = new LinkedList<Literal>();
      l5.add(new Literal(2, 'x'));
      l5.add(new Literal(1, 'y'));
      l5.add(new Literal(4, 'z'));
      l5.add(new Literal(1, 'a'));
      Monomio mo5 = new Monomio(2, l5);
      Assert.assertEquals(mo3.multiplica(mo4), mo5);

    }

}//class
